﻿using DTO;

namespace Match
{
    public class Player
    {
        public IReadOnlyCollection<CharacterDTO> Characters { get => characters.AsReadOnly(); }
        public bool Alive { get => characters.Any(x => x.Hp > 0); }
        public bool Ready { get => characters.Count == 3; }
        public string Name { get; }
        public CharacterDTO? Current { get => characters.Find(x => x.Hp > 0); }

        private readonly List<CharacterDTO> characters;

        public Player(string name)
        {
            Name = name;
            characters = new List<CharacterDTO>();
        }

        public bool AddCharacter(CharacterDTO character)
        {
            if (characters.Count > 3) return false;
            var found = characters.Find(x => x.CharacterName == character.CharacterName);
            if (found == null)
                characters.Add(character);
            return found == null;
        }
    }
}
