using DTO;
using Match.Exceptions;

namespace Match
{
    public class Game
    {
        public string Code { get; }
        public IEnumerable<Player> Players { get => players.AsReadOnly(); }
        public bool Started { get; private set; }
        public bool Ready { get => players.Count == 2; }

        private readonly List<Player> players;

        public Game(string code)
        {
            this.Code = code;
            players = new List<Player>();
        }


        public bool AddPlayer(string name)
        {
            if (players.Count >= 2) return false;
            if (players.Find(p => p.Name == name) != null) return false;
            players.Add(new Player(name));
            return true;
        }

        public void Start()
        {
            if (!Ready)
                throw new NotEnoughPlayersException(players.Count);
            else
            {
                if (!players[0].Ready)
                    throw new NotEnoughCharactersException(players[0].Name, players[0].Characters.Count);
                if (!players[1].Ready)
                    throw new NotEnoughCharactersException(players[1].Name, players[1].Characters.Count);
                else
                    Started = true;
            }
        }

        public bool MakeMove(string attackerPlayerName, string attackName)
        {
            if (!Started) return false;
            var player = players.Find(x => x.Name == attackerPlayerName);
            if (player == null) return false;

            var enemyPlayer = players.Find(y => y.Name != attackerPlayerName);
            if (enemyPlayer == null) return false;

            var character = player.Current;
            if (character == null) return false;

            var enemyCharacter = enemyPlayer.Current;
            if (enemyCharacter == null) return false;

            var attack = character.Attacks.ToList().Find(x => x.AttackName == attackName);
            if (attack == null) return false;


            if (attack.Available < DateTime.Now)
            {
                var cooldown = attack.Cooldown;
                attack.Available = DateTime.Now.AddSeconds(cooldown);
                Attack(enemyPlayer, attack, enemyCharacter);
            }
            return true;
        }

        private void Attack(Player enemyPlayer, AttackDTO attack, CharacterDTO enemyCharacter)
        {
            if (enemyPlayer.Current != null)
            {
                var luck = Random.Shared.NextDouble();

                if (luck <= attack.Precision)
                {
                    enemyPlayer.Current.Hp -= (attack.Power * enemyCharacter.Defense);
                }
            }
        }
    }
}
