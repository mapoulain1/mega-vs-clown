﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match.Exceptions
{
    public class NotEnoughCharactersException : Exception
    {
        public string PlayerName { get; }
        public int NumberOfCharacters { get; }
        public override string Message => $"{PlayerName} has only {NumberOfCharacters} characters out of 3";

        public NotEnoughCharactersException(string playerName, int numberOfCharacters)
        {
            PlayerName = playerName;
            NumberOfCharacters = numberOfCharacters;
        }

        
    }
}
