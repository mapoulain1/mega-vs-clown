﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match.Exceptions
{
    public class NotEnoughPlayersException : Exception
    {
        public int NumberOfPlayer { get; }

        public override string Message => $"Game has only {NumberOfPlayer} players out of 2";
        public NotEnoughPlayersException(int numberOfPlayer)
        {
            NumberOfPlayer = numberOfPlayer;
        }
    }
}
