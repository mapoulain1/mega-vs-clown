﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;
using DTO;

namespace Data
{
    public static class Extension
    {

        public static List<CharacterDTO> ToDTO(this List<Character> list)
        {
            if (list == null && !list.Any())
                throw new ArgumentNullException(nameof(list));
            var result = new List<CharacterDTO>();
            list.ForEach(x => result.Add(x.ToDTO()));
            return result;
        }

        public static List<ArenaDTO> ToDTO(this List<Arena> list)
        {
            if (list == null && !list.Any())
                throw new ArgumentNullException(nameof(list));
            var result = new List<ArenaDTO>();
            list.ForEach(x => result.Add(x.ToDTO()));
            return result;
        }

        public static List<AttackDTO> ToDTO(this List<Attack> list)
        {
            if (list == null && !list.Any())
                throw new ArgumentNullException(nameof(list));
            var result = new List<AttackDTO>();
            list.ForEach(x => result.Add(x.ToDTO()));
            return result;
        }



        public static CharacterDTO ToDTO(this Character character)
        {
            if (character == null)
                throw new ArgumentNullException(nameof(character), "can't be null");
            var attacks = new List<AttackDTO>();

            if (character.AttackName1Navigation != null) attacks.Add(character.AttackName1Navigation.ToDTO());
            if (character.AttackName2Navigation != null) attacks.Add(character.AttackName2Navigation.ToDTO());
            if (character.AttackName3Navigation != null) attacks.Add(character.AttackName3Navigation.ToDTO());
            if (character.AttackName4Navigation != null) attacks.Add(character.AttackName4Navigation.ToDTO());

            return new CharacterDTO()
            {
                CharacterName = character.CharacterName,
                Sprite = character.Sprite,
                FullSprite = character.FullSprite == null ? character.Sprite : character.FullSprite,
                Defense = character.Defense,
                Hp = character.Hp,
                HpMax = character.Hp,
                Attacks = attacks,
            };
        }

        public static AttackDTO ToDTO(this Attack attack)
        {
            if (attack == null)
                throw new ArgumentNullException(nameof(attack), "can't be null");
            return new AttackDTO()
            {
                Id = attack.Id,
                AttackName = attack.AttackName,
                Description = attack.Description,
                Power = attack.Power,
                Precision = attack.Precision,
                Cooldown = attack.Cooldown
            };
        }   

        public static ArenaDTO ToDTO(this Arena arena)
        {
            if (arena == null)
                throw new ArgumentNullException(nameof(arena), "can't be null");
            return new ArenaDTO()
            {
                ArenaName = arena.ArenaName,
                Sprite = arena.Sprite,
            };
        }
    }
}
