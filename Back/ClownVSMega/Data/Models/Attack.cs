﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Attack
    {
        public Attack()
        {
            CharacterAttackName1Navigations = new HashSet<Character>();
            CharacterAttackName2Navigations = new HashSet<Character>();
            CharacterAttackName3Navigations = new HashSet<Character>();
            CharacterAttackName4Navigations = new HashSet<Character>();
        }

        public int Id { get; set; }
        public string AttackName { get; set; } = null!;
        public string? Description { get; set; }
        public double Power { get; set; }
        public double Precision { get; set; }
        public double Cooldown { get; set; }

        public virtual ICollection<Character> CharacterAttackName1Navigations { get; set; }
        public virtual ICollection<Character> CharacterAttackName2Navigations { get; set; }
        public virtual ICollection<Character> CharacterAttackName3Navigations { get; set; }
        public virtual ICollection<Character> CharacterAttackName4Navigations { get; set; }
    }
}
