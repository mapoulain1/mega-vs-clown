﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Arena
    {
        public string ArenaName { get; set; } = null!;
        public string Sprite { get; set; } = null!;
    }
}
