﻿namespace DTO
{
    public class AttackDTO
    {
        public int Id { get; set; }
        public string AttackName { get; set; } = null!;
        public string? Description { get; set; }
        public double Power { get; set; }
        public double Precision { get; set; }
        public double Cooldown { get; set; }
        public DateTime Available { get; set; } = DateTime.MinValue;


    }
}
