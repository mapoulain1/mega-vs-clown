﻿using Data;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public partial class AdminController : ControllerBase
    {
        private readonly DatabaseContext context;

        public AdminController(DatabaseContext _context)
        {
            context = _context;
        }
    }
}
