﻿using Data;
using DTO;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ArenaController : ControllerBase
    {
        private readonly DatabaseContext context;

        public ArenaController(DatabaseContext _context)
        {
            context = _context;
        }

        [HttpGet("GetArenas")]
        public List<ArenaDTO> GetArenas()
        {
            return context.Arenas.ToList().ToDTO();
        }


        [HttpGet("GetArena")]
        public ArenaDTO GetArena(string name)
        {
            var found = context.Arenas.ToList().FirstOrDefault(x => x.ArenaName == name);
            return found?.ToDTO();
        }




    }
}
