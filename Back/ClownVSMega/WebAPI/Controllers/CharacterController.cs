﻿using Data;
using DTO;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CharacterController : ControllerBase
    {
        private readonly DatabaseContext context;
        private readonly int ChunckSize = 19;

        public CharacterController(DatabaseContext _context)
        {
            context = _context;
        }

        [HttpGet("GetCharacters")]
        public List<CharacterDTO> GetCharacters(int page)
        {
            try
            {
                var characters = context.Characters.ToList();
                return characters.Chunk(ChunckSize).ToList()[page].ToList().ToDTO();
            }
            catch (Exception) { return new List<CharacterDTO>(); }
        }

        [HttpGet("GetCharacter")]
        public CharacterDTO GetCharacter(string name)
        {
            var found = context.Characters.ToList().FirstOrDefault(x => x.CharacterName == name);
            return found?.ToDTO();
        }

        [HttpGet("GetPages")]
        public List<int> GetPages()
        {
            var numberOfChunk = context.Characters.Count() / ChunckSize + 1;
            var pages = new List<int>();
            for (int i = 0; i < numberOfChunk; i++)
                pages.Add(i);
            return pages;
        }


    }
}
