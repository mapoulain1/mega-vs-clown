﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public partial class AdminController
    {

        [HttpPost("AdminAddArena")]
        public IActionResult AdminAddArena(string name, string imagePath)
        {
            try
            {
                if (!imagePath.Contains(".png") && !imagePath.Contains(".jpg"))
                    return BadRequest($"image path <{imagePath}> must finish with \".png\" or  \".jpg\"");
                var arena = new Arena
                {
                    ArenaName = name,
                    Sprite = imagePath
                };
                context.Arenas.Add(arena);
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


        [HttpPut("AdminUpdateArena")]
        public IActionResult AdminUpdateArena(string name, string newName, string imagePath)
        {
            try
            {
                if (!imagePath.Contains(".png") && !imagePath.Contains(".jpg"))
                    return BadRequest("image path must finish with \".png\" or  \".jpg\"");
                var arena = context.Arenas.Single(x => x.ArenaName == name);
                if (arena == null)
                    return BadRequest($"arena <{name}> not found");
                arena.ArenaName = newName;
                arena.Sprite = imagePath;
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


        [HttpDelete("AdminDeleteArena")]
        public IActionResult AdminDeleteArena(string name)
        {
            try
            {
                var arena = context.Arenas.Single(x => x.ArenaName == name);
                if (arena == null)
                    return BadRequest($"arena <{name}> not found");
                context.Arenas.Remove(arena);
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


    }
}
