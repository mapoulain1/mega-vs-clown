﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public partial class AdminController
    {

        [HttpPost("AdminAddCharacter")]
        public IActionResult AdminAddCharacter(string name, double hp, double defense, string sprite, string fullSprite, int? attack1, int? attack2, int? attack3, int? attack4)
        {
            try
            {
                if (!sprite.Contains(".png") && !sprite.Contains(".jpg"))
                    return BadRequest($"image path <{sprite}> must finish with \".png\" or  \".jpg\"");
                var character = new Character()
                {
                    CharacterName = name,
                    Hp = hp,
                    Defense = defense,
                    Sprite = sprite,
                    FullSprite = fullSprite,
                    AttackName1 = attack1,
                    AttackName2 = attack2,
                    AttackName3 = attack3,
                    AttackName4 = attack4,
                };
                context.Characters.Add(character);
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


        [HttpPut("AdminUpdateCharacter")]
        public IActionResult AdminUpdateCharacter(string name, string newName, double hp, double defense, string sprite, int? attack1, int? attack2, int? attack3, int? attack4)
        {
            try
            {
                if (!sprite.Contains(".png") && !sprite.Contains(".jpg"))
                    return BadRequest($"image path <{sprite}> must finish with \".png\" or  \".jpg\"");
                var character = context.Characters.Single(x => x.CharacterName == name);
                if (character == null)
                    return BadRequest($"character <{name}> not found");
                character.CharacterName = newName;
                character.Hp = hp;
                character.Defense = defense;
                character.Sprite = sprite;
                character.AttackName1 = attack1;
                character.AttackName2 = attack2;
                character.AttackName3 = attack3;
                character.AttackName4 = attack4;
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


        [HttpDelete("AdminDeleteCharacter")]
        public IActionResult AdminDeleteCharacter(string name)
        {
            try
            {
                var character = context.Characters.Single(x => x.CharacterName == name);
                if (character == null)
                    return BadRequest($"character <{name}> not found");
                context.Characters.Remove(character);
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }

    }
}
