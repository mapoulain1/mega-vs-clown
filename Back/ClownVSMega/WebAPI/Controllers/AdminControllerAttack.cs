﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public partial class AdminController
    {

        [HttpPost("AdminAddAttack")]
        public IActionResult AdminAddAttack(int id, string name, string description, double power, double precision, double cooldown)
        {
            try
            {
                var attack = new Attack()
                {
                    Id = id,
                    AttackName = name,
                    Description = description,
                    Power = power,
                    Precision = precision,
                    Cooldown = cooldown,
                };
                context.Attacks.Add(attack);
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


        [HttpPut("AdminUpdateAttack")]
        public IActionResult AdminUpdateAttack(int id, string name, string description, double power, double precision, double cooldown)
        {
            try
            {
                var attack = context.Attacks.Single(x => x.Id == id);
                if (attack == null)
                    return BadRequest($"attack <{id}> not found");
                attack.AttackName = name;
                attack.Power = power;
                attack.Precision = precision;
                attack.Description = description;
                attack.Cooldown = cooldown;

                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


        [HttpDelete("AdminDeleteAttack")]
        public IActionResult AdminDeleteAttack(int id)
        {
            try
            {
                var attack = context.Attacks.Single(x => x.Id == id);
                if (attack == null)
                    return BadRequest($"attack <{id}> not found");
                context.Attacks.Remove(attack);
                context.SaveChanges();
            }
            catch (Exception e) { return Problem(e.Message); }
            return Ok();
        }


    }
}
