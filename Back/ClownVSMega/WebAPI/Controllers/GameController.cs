﻿using Data;
using Match;
using Match.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private readonly DatabaseContext context;

        public GameController(DatabaseContext _context)
        {
            context = _context;
        }




        [HttpPost("CreateGame")]
        public string CreateGame()
        {
            return GameManager.Instance.Create();

        }

        [HttpGet("IsGameExisting")]
        public bool IsGameExisting(string code)
        {
            return GameManager.Instance.Existe(code);
        }

        [HttpPost("AddPlayer")]
        public IActionResult AddPlayer(string code, string name)
        {
            if (!GameManager.Instance.Existe(code)) return BadRequest("Game must be created before adding players");
            return GameManager.Instance.AddPlayer(code, name) ? Ok() : BadRequest($"Can't add player {name}");
        }

        [HttpPut("StartGame")]
        public IActionResult StartGame(string code)
        {
            if (!GameManager.Instance.Existe(code)) return BadRequest("Game must be created before starting game");
            if (!GameManager.Instance.Ready(code)) return BadRequest("Game must contain 2 players before starting");
            try
            {
                GameManager.Instance.Start(code);
            }
            catch (NotEnoughPlayersException ex) { return BadRequest(ex.Message); }
            catch (NotEnoughCharactersException ex) { return BadRequest(ex.Message); }
            return GameManager.Instance.IsStarted(code) ? Ok() : BadRequest("Unknown error occured");
        }


        [HttpPut("StopGame")]
        public IActionResult StopGame(string code)
        {
            GameManager.Instance.Stop(code);
            return Ok();
        }


        [HttpPost("AddCharacter")]
        public IActionResult AddCharacter(string code, string playerName, string characterName)
        {
            if (!GameManager.Instance.Existe(code)) return BadRequest("Game must be created before adding characters");
            try
            {
                var character = context.Characters.First(x => x.CharacterName == characterName);
                var result = GameManager.Instance.AddCharacter(code, playerName, character.ToDTO());
                return result ? Ok() : BadRequest($"can't add {characterName} to {playerName}");
            }
            catch (InvalidOperationException) { return BadRequest("Unknown character"); }
        }


        [HttpGet("GetPlayers")]
        public IEnumerable<Player> GetPlayers(string code)
        {
            if (!GameManager.Instance.Existe(code)) return Enumerable.Empty<Player>();
            return GameManager.Instance.GetPlayers(code);
        }

        [HttpPost("MakeMove")]
        public IActionResult MakeMove(string code, string attackerPlayerName, string attackName)
        {
            var res = GameManager.Instance.MakeMove(code, attackerPlayerName, attackName);
            return res ? Ok() : BadRequest("Can't make move");
        }

        [HttpGet("GetWinner")]
        public string GetWinner(string code)
        {
            if (!GameManager.Instance.Existe(code)) return string.Empty;
            if (!GameManager.Instance.IsStarted(code)) return string.Empty;
            return GameManager.Instance.GetWinner(code);
        }

    }
}
