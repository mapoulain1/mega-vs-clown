drop table character;
drop table attack;
drop table arena;


create table attack
(
    id int primary key,
    attack_name varchar(50) not null,
    description varchar(500),
    power float not null,
    precision float not null,
    cooldown float not null,
);

create table character
(
    character_name varchar(50) primary key,
    hp float not null,
    defense float not null,
    sprite varchar(300),
    full_sprite varchar(300),
    attack_name_1 int constraint "fk_character_attack1" references attack(id),
    attack_name_2 int constraint "fk_character_attack2" references attack(id),
    attack_name_3 int constraint "fk_character_attack3" references attack(id),
    attack_name_4 int constraint "fk_character_attack4" references attack(id),
);

create table arena
(
    arena_name varchar(50) primary key,
    sprite varchar(300) not null,
);


insert into attack values(1, 'Abattage','Le lanceur balaie violemment le camp adverse avec son immense queue. Baisse l''Attaque de la cible', 5, 1, 1);
insert into attack values(2, 'Abîme','Le lanceur fait tomber l''ennemi dans une crevasse. Si cette attaque réussit, elle met K.O. sur le coup', 7, 1, 2);
insert into attack values(3, 'Aboiement','Le lanceur hurle sur l''ennemi. Baisse l''Attaque Spéciale de l''ennemi', 12, 1, 8);
insert into attack values(4, 'Abri','Le lanceur se protège de toutes les attaques. Peut échouer si utilisée plusieurs fois de suite', 1, 1, 0.5);
insert into attack values(5, 'Acidarmure','Le lanceur modifie sa structure moléculaire pour se liquéfier et beaucoup augmenter sa Défense', 5, 1, 3);
insert into attack values(6, 'Acide','Le lanceur attaque l''ennemi avec un jet d''acide corrosif. Peut aussi baisser sa Défense Spéciale', 20, 1, 5);
insert into attack values(7, 'Falcon Punch','Le lanceur mange ses morts à l''adversaire', 20, 1, 8);
insert into attack values(8, 'Fus Roh Dah','Une incantation en langue de dragon qui produit un un puissant souffle', 10, 1, 4);
insert into attack values(9, 'Explosion','Le lanceur explose et inflige des dégâts à tous les Pokémon autour de lui. Met K.O. le lanceur.', 10, 1, 3);
insert into attack values(10, 'Giga-Tonnerre','Le Pikachu à casquette utilise la Force Z pour augmenter sa puissance électrique avant de la déchaîner sur la cible. Taux de critique élevé.', 8, 1, 2);
insert into attack values(11, 'Élecanon','100% de chance de paralyser la cible.', 30, 1, 4);

                                                                                                                                                     
insert into character values('Bayonetta',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/bayonetta.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/63/01.png',1,2,3,null);
insert into character values('Bowser',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/bowser.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/14/01.png',1,5,3,4);
insert into character values('Bowser JR',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/bowser_jr.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/58/01.png',1,7,3,8);
insert into character values('Captain Falcon',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/captain_falcon.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/11/01.png',3,2,7,4);
insert into character values('Chrom',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/chrom.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/25e/01.png',9,11,3,8);
insert into character values('Cloud',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/cloud.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/61/01.png',10,4,6,1);
insert into character values('Corrin',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/corrin.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/62/01.png',5,2,3,4);
insert into character values('Daisy',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/daisy.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/13e/01.png',11,2,3,4);
insert into character values('Dark Pit',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/dark_pit.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/28e/01.png',9,2,3,4);
insert into character values('Dark Samus',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/dark_samus.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/04e/01.png',8,7,3,4);
insert into character values('Diddy Kong',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/diddy_kong.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/36/01.png',1,6,3,7);
insert into character values('Donkey Kong',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/donkey_kong.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/02/01.png',5,2,3,4);
insert into character values('Dr Mario',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/dr_mario.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/18/01.png',11,2,10,4);
insert into character values('Duck Hunt',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/duck_hunt.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/59/01.png',8,2,3,4);
insert into character values('Falco',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/falco.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/20/01.png',7,2,3,4);
insert into character values('Fox',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/fox.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/07/01.png',1,3,7,4);
insert into character values('Ganondorf',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/ganondorf.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/23/01.png',10,2,3,8);
insert into character values('Greninja',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/greninja.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/50/01.png',4,2,3,10);
insert into character values('Ice Climbers',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/ice_climbers.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/15/01.png',10,11,3,4);
insert into character values('Ike',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/ike.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/32/01.png',1,8,3,4);
insert into character values('Incineroar',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/incineroar.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/69/01.png',1,2,3,4);
insert into character values('Inkling',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/inkling.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/64/01.png',1,2,7,4);
insert into character values('Isabelle',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/isabelle.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/68/01.png',1,6,3,4);
insert into character values('Jigglypuff',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/jigglypuff.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/12/01.png',10,2,3,4);
insert into character values('Ken',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/ken.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/60/01.png',1,2,3,4);
insert into character values('King Dedede',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/king_dedede.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/39/01.png',11,2,3,4);
insert into character values('King K Rool',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/king_k_rool.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/67/01.png',1,2,3,4);
insert into character values('Kirby',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/kirby.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/06/01.png',1,2,3,9);
insert into character values('Link',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/link.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/03/01.png',1,2,3,4);
insert into character values('Little Mac',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/little_mac.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/49/01.png',1,2,3,4);
insert into character values('Lucario',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/lucario.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/41/01.png',1,2,3,4);
insert into character values('Lucas',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/lucas.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/37/01.png',1,2,3,4);
insert into character values('Lucina',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/lucina.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/21e/01.png',1,2,3,4);
insert into character values('Luigi',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/luigi.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/09/01.png',1,2,3,4);
insert into character values('Mario',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/mario.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/01/01.png',1,2,3,4);
insert into character values('Marth',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/marth.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/21/01.png',1,2,3,4);
insert into character values('Mega Man',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/mega_man.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/46/01.png',1,2,3,4);
insert into character values('Meta Knight',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/meta_knight.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/27/01.png',1,2,3,4);
insert into character values('Mewtwo',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/mewtwo.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/24/01.png',1,2,3,4);
insert into character values('Mii Brawler',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/mii_brawler.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/51/01.png',1,2,3,4);
insert into character values('Mii Gunner',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/mii_gunner.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/53/01.png',1,2,3,4);
insert into character values('Mii Swordfighter',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/mii_swordfighter.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/52/01.png',1,2,3,4);
insert into character values('Mr Game and Watch',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/mr_game_and_watch.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/26/01.png',1,2,3,4);
insert into character values('Ness',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/ness.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/10/01.png',1,2,3,4);
insert into character values('Olimar',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/olimar.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/40/01.png',1,2,3,4);
insert into character values('Pac Man',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/pac_man.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/55/01.png',1,2,3,4);
insert into character values('Palutena',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/palutena.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/54/01.png',1,2,3,4);
insert into character values('Peach',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/peach.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/13/01.png',1,2,3,4);
insert into character values('Pichu',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/pichu.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/19/01.png',1,2,3,4);
insert into character values('Pikachu',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/pikachu.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/08/01.png',1,2,3,4);
insert into character values('Piranha Plant',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/piranha_plant.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/70/01.png',1,2,3,4);
insert into character values('Pit',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/pit.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/28/01.png',1,2,3,4);
insert into character values('Pokemon Trainer',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/pokemon_trainer.png','https://img.rankedboost.com/wp-content/plugins/super-smash-bros-ultimate/assets/character-images-main/Pokemon_Trainer_SSBU.png',1,2,3,4);
insert into character values('Richter',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/richter.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/66e/01.png',1,2,3,4);
insert into character values('Ridley',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/ridley.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/65/01.png',1,2,3,4);
insert into character values('Rob',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/rob.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/42/01.png',1,2,3,4);
insert into character values('Robin',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/robin.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/56/01.png',1,2,3,4);
insert into character values('Rosalina and Luma',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/rosalina_and_luma.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/48/01.png',1,2,3,4);
insert into character values('Roy',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/roy.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/25/01.png',1,2,3,4);
insert into character values('Ryu',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/ryu.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/60/01.png',1,2,3,4);
insert into character values('Samus',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/samus.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/04/01.png',1,2,3,4);
insert into character values('Sheik',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/sheik.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/16/01.png',1,2,3,4);
insert into character values('Shulk',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/shulk.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/57/01.png',1,2,3,4);
insert into character values('Simon',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/simon.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/66/01.png',1,2,3,4);
insert into character values('Snake',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/snake.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/31/01.png',1,2,3,4);
insert into character values('Sonic',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/sonic.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/38/01.png',1,2,3,4);
insert into character values('Toon Link',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/toon_link.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/43/01.png',1,2,3,4);
insert into character values('Villager',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/villager.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/45/01.png',1,2,3,4);
insert into character values('Wario',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/wario.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/30/01.png',1,2,3,4);
insert into character values('Wii Fit Trainer',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/wii_fit_trainer.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/47/01.png',1,2,3,4);
insert into character values('Wolf',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/wolf.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/44/01.png',1,2,3,4);
insert into character values('Yoshi',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/yoshi.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/05/01.png',1,2,3,4);
insert into character values('Young Link',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/young_link.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/22/01.png',1,2,3,4);
insert into character values('Zelda',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/zelda.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/17/01.png',1,2,3,4);
insert into character values('Zero Suit Samus',100,1,'https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/portraits/full/zero_suit_samus.png','https://raw.githubusercontent.com/marcrd/smash-ultimate-assets/master/renders/fighters/29/01.png',1,2,3,4);



insert into arena values('The Virtual Reach','https://i.ibb.co/2spkLxB/arena1.gif');
insert into arena values('The Moving Fields','https://i.ibb.co/j4MFSMs/arena2.gif');
insert into arena values('The Abyss Dominion','https://i.ibb.co/tKm12sJ/arena3.gif');
insert into arena values('The Amber Forest','https://i.ibb.co/XxLrrfq/arena4.gif');
insert into arena values('The Cyber Lake','https://i.ibb.co/d5XnszZ/arena5.gif');
insert into arena values('The Center Universe','https://i.ibb.co/ckcmrzD/arena6.gif');
insert into arena values('The Empty Land','https://i.ibb.co/N7pRdb1/arena7.gif');
insert into arena values('The Gentle Reach','https://i.ibb.co/2j6F5PL/arena8.gif');
insert into arena values('The Limbo Sanctum','https://i.ibb.co/SXxmSbS/arena9.gif');
insert into arena values('The Lonely Domain','https://i.ibb.co/gW5xVgB/arena10.gif');
insert into arena values('The Lunar Territories','https://i.ibb.co/VxgF2xk/arena11.gif');
insert into arena values('The Mirage Moon','https://i.ibb.co/C68kYYj/arena12.gif');
insert into arena values('The Perpetual Country','https://i.ibb.co/QKLKdpN/arena13.gif');
insert into arena values('The Perpetual Domain','https://i.ibb.co/7zmD5y7/arena14.gif');
insert into arena values('The Shattered Forest','https://i.ibb.co/Dk2570T/arena15.gif');
insert into arena values('The Specter Sea','https://i.ibb.co/FsH830c/arena16.gif');
