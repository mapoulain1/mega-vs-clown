export default class Attack {
  id: number = -1;
  attackName: string = '';
  description: string = '';
  power: number = 0;
  precision: number = 1;
  cooldown: number = 1;

  constructor(
    id: number,
    attackName: string,
    description: string,
    power: number,
    precision: number,
    cooldown: number
  ) {
    this.id = id;
    this.attackName = attackName;
    this.description = description;
    this.power = power;
    this.precision = precision;
    this.cooldown = cooldown;
  }
}
