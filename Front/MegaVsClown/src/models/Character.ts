import Attack from './Attack';

export default class Character {
  characterName: string = '';
  hp: number = 1;
  hpMax: number = 1;
  defense: number = 1;
  sprite: string = '';
  fullSprite: string = '';
  attacks: Attack[] = [];

  constructor(
    characterName: string,
    hp: number,
    defense: number,
    sprite: string,
    fullSprite: string,
    attacks: Attack[]
  ) {
    this.characterName = characterName;
    this.hp = hp;
    this.hpMax = hp;
    this.defense = defense;
    this.sprite = sprite;
    this.fullSprite = fullSprite;
    this.attacks = attacks;
  }
}
