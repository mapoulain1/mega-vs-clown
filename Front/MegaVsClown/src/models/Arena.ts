export default class Arena {
  arenaName: string = "";
  sprite: string = "";

  constructor(arenaName: string, sprite: string) {
    this.arenaName = arenaName;
    this.sprite = sprite;
  }
}
