import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Character from 'src/models/Character';
import { CharacterService } from '../character.service';
import { GameService } from '../game.service';
import { loadingCharacter, choosingCharacter, playerName } from '../global';

@Component({
  selector: 'app-character-chooser',
  templateUrl: './character-chooser.component.html',
  styleUrls: ['./character-chooser.component.css'],
})
export class CharacterChooserComponent implements OnInit {
  @Input() readyToPlay: boolean = false;
  @Input() playerName: string = playerName;

  characters: Character[];
  choosenCharacters: Character[] = [
    choosingCharacter,
    choosingCharacter,
    choosingCharacter,
  ];
  characterService: CharacterService;
  gameService: GameService;
  indexChoosen: number = 0;

  constructor(
    private router: Router,
    characterService: CharacterService,
    gameService: GameService
  ) {
    this.characterService = characterService;
    this.gameService = gameService;
    this.characters = new Array<Character>(30).fill(loadingCharacter);
  }

  ngOnInit(): void {
    if (playerName === '') {
      this.router.navigate(['']);
    }
    this.characterService
      .getCharacters(0)
      .subscribe((characters) => (this.characters = characters));
  }

  onCharacterAdded(character: Character) {
    if (this.indexChoosen < 3) {
      if (
        !this.choosenCharacters.find(
          (x) => x.characterName === character.characterName
        )
      ) {
        this.choosenCharacters[this.indexChoosen] = character;
        this.indexChoosen++;
        this.gameService.addCharacter(character);
        this.readyToPlay = this.indexChoosen == 3;
      }
    }
  }
  playClicked() {
    this.router.navigate(['arenaSelection']);
  }
}
