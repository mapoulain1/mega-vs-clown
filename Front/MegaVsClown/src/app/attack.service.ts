import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Attack from 'src/models/Attack';
import { API_PATH } from './global';

@Injectable({
  providedIn: 'root'
})
export class AttackService {

  constructor(private http: HttpClient) { }

  
  getAttacks(): Observable<Attack[]> {
    return this.http.get<Attack[]>(`${API_PATH}/Attack/GetAttacks`);
  }
}
