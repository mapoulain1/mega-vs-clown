import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Arena from 'src/models/Arena';
import { API_PATH } from './global';

@Injectable({
  providedIn: 'root',
})
export class ArenaService {
  constructor(private http: HttpClient) {}

  getArenas(): Observable<Arena[]> {
    return this.http.get<Arena[]>(`${API_PATH}/Arena/GetArenas`);
  }
}
