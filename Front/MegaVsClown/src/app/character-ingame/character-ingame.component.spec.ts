import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterIngameComponent } from './character-ingame.component';

describe('CharacterIngameComponent', () => {
  let component: CharacterIngameComponent;
  let fixture: ComponentFixture<CharacterIngameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterIngameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterIngameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
