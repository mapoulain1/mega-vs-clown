import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Observable } from 'rxjs';
import Character from 'src/models/Character';
import { loadingCharacter } from '../global';

@Component({
  selector: 'app-character-ingame',
  templateUrl: './character-ingame.component.html',
  styleUrls: ['./character-ingame.component.css'],
})
export class CharacterIngameComponent implements OnInit {
  @Input() character: Character = loadingCharacter;
  @Input() position: string = 'left';

  healthPercent: number = 100;
  healthPercentOld: number = 100;
  toggleShake: boolean = false;
  toggleDashLeft: boolean = false;
  toggleDashRight: boolean = false;
  dashAnimation: string = 'dashRightAnimation';
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChange) {
    this.updateHealth();
  }

  updateHealth() {
    this.healthPercent = (this.character.hp / this.character.hpMax) * 100;
    if (this.healthPercent < this.healthPercentOld)
      this.shake();
    this.healthPercentOld = this.healthPercent;
  }

  shake() {
    this.toggleShake = !this.toggleShake;
    setTimeout(() => this.toggleShake = !this.toggleShake, 100);
  }

  dash() {
    if (this.position === 'left') {
      this.toggleDashRight = !this.toggleDashRight;
      setTimeout(() => this.toggleDashRight = !this.toggleDashRight, 100);
    }
    else {
      this.toggleDashLeft = !this.toggleDashLeft;
      setTimeout(() => this.toggleDashLeft = !this.toggleDashLeft, 100);
    }

  }
}
