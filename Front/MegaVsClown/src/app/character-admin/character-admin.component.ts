import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import Attack from 'src/models/Attack';
import Character from 'src/models/Character';
import { AdminService } from '../admin.service';
import { AttackService } from '../attack.service';
import { fourAttacks, loadingAttack, loadingCharacter } from '../global';

@Component({
  selector: 'app-character-admin',
  templateUrl: './character-admin.component.html',
  styleUrls: ['./character-admin.component.css'],
})
export class CharacterAdminComponent implements OnInit, OnChanges {
  @Output() onUpdate: EventEmitter<any> = new EventEmitter();
  @Input() character: Character = loadingCharacter;
  disabled: boolean = false;
  deleteVerification: boolean = false;
  fourAttacks: Attack[] = fourAttacks;
  allAttacks: Attack[] = [loadingAttack];

  constructor(
    private adminService: AdminService,
    private attackService: AttackService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.disabled = this.character === loadingCharacter;
  }

  ngOnInit(): void {
    this.attackService
      .getAttacks()
      .subscribe((attacks) => (this.allAttacks = attacks));
  }

  updateCharacter(
    name: string,
    health: number,
    defense: number,
    sprite: string,
    fullSprite: string
  ): void {
    this.adminService
      .updateCharacter(
        this.character.characterName,
        name,
        health,
        defense,
        sprite,
        fullSprite,
        this.character.attacks[0].id,
        this.character.attacks[1].id,
        this.character.attacks[2].id,
        this.character.attacks[3].id
      )
      .subscribe(() => this.onUpdate.emit(),() => this.onUpdate.emit());
  }

  deleteCharacter(): void {
    this.deleteVerification = !this.deleteVerification;
    if (!this.deleteVerification) {
      this.adminService
        .deleteCharacter(this.character.characterName)
        .subscribe(() => this.onUpdate.emit());
    }
  }

  selectChanged(index: number, event: any): void {
    let found = this.allAttacks.find((x) => x.id === +event.target.value);
    if (found !== undefined) {
      this.character.attacks[index] = found;
      console.log(`attack ${index} is now `, found.attackName);
    }
  }
}
