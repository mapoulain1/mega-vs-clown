import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Arena from 'src/models/Arena';
import { ArenaService } from '../arena.service';
import { loadingArena, selectedArena, setSelectedArena } from '../global';

@Component({
  selector: 'app-arena-chooser',
  templateUrl: './arena-chooser.component.html',
  styleUrls: ['./arena-chooser.component.css'],
})
export class ArenaChooserComponent implements OnInit {
  arenaService: ArenaService;
  arenas: Arena[];

  constructor(private router: Router, arenaService: ArenaService) {
    this.arenaService = arenaService;
    this.arenas = new Array<Arena>(10).fill(loadingArena);
  }

  ngOnInit(): void {
    setSelectedArena(loadingArena);
    this.arenaService.getArenas().subscribe((arenas) => {
      this.arenas = arenas;
      setSelectedArena(arenas[0]);
    });
  }
  playClicked() {
    if (selectedArena.arenaName !== loadingArena.arenaName) {
      this.router.navigate(['/fight']);
    }
  }
  onSlide(event: any) {
    const index = parseInt(event.current.replace('ngb-slide-1', ''));
    setSelectedArena(this.arenas[index]);
  }
}
