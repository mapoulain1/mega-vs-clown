import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArenaChooserComponent } from './arena-chooser.component';

describe('ArenaChooserComponent', () => {
  let component: ArenaChooserComponent;
  let fixture: ComponentFixture<ArenaChooserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArenaChooserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArenaChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
