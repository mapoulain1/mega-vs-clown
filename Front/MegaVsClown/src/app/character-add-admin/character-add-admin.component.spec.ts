import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterAddAdminComponent } from './character-add-admin.component';

describe('CharacterAddAdminComponent', () => {
  let component: CharacterAddAdminComponent;
  let fixture: ComponentFixture<CharacterAddAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterAddAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterAddAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
