import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import Attack from 'src/models/Attack';
import { AdminService } from '../admin.service';
import { AttackService } from '../attack.service';
import { loadingAttack } from '../global';

@Component({
  selector: 'app-character-add-admin',
  templateUrl: './character-add-admin.component.html',
  styleUrls: ['./character-add-admin.component.css'],
})
export class CharacterAddAdminComponent implements OnInit {
  @Output() onUpdate: EventEmitter<any> = new EventEmitter();
  attacks: Attack[];
  characterAttack: Attack[] = [
    loadingAttack,
    loadingAttack,
    loadingAttack,
    loadingAttack,
  ];

  constructor(
    private adminService: AdminService,
    private attackService: AttackService
  ) {
    this.attacks = [loadingAttack, loadingAttack, loadingAttack, loadingAttack];
    attackService.getAttacks().subscribe((attacks) => (this.attacks = attacks));
  }

  ngOnInit(): void {}

  addCharacter(
    name: string,
    health: number,
    defense: number,
    sprite: string,
    fullSprite: string
  ): void {
    this.adminService
      .addCharacter(
        name,
        health,
        defense,
        sprite,
        fullSprite,
        this.characterAttack[0].id,
        this.characterAttack[1].id,
        this.characterAttack[2].id,
        this.characterAttack[3].id
      )
      .subscribe(() => this.onUpdate.emit());
  }
  selectChanged(index: number, event: any): void {
    let found = this.attacks.find((x) => x.id === +event.target.value);
    if (found !== undefined) {
      this.characterAttack[index] = found;
      console.log(`attack ${index} is now `, found.attackName);
    }
  }
}
