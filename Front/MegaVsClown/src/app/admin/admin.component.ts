import { Component, OnInit } from '@angular/core';
import Character from 'src/models/Character';
import { CharacterService } from '../character.service';
import { GameService } from '../game.service';
import { loadingCharacter } from '../global';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  characters: Character[];
  charactersLoaded: boolean = false;
  characterService: CharacterService;
  indexPages: number[];
  currentIndexPage: number;
  loadingCharacters: Character[];

  constructor(characterService: CharacterService) {
    this.characterService = characterService;
    this.loadingCharacters = new Array<Character>(30).fill(loadingCharacter);
    this.characters = this.loadingCharacters;
    this.indexPages = [0];
    this.currentIndexPage = 0;
  }

  ngOnInit(): void {
    this.getCharacters();
  }

  charactersUpdated(): void {
    this.getCharacters();
  }

  getCharacters(): void {
    this.characterService.getCharacters(this.currentIndexPage).subscribe((characters) => { this.characters = characters; this.charactersLoaded = true });
    this.characterService.getPages().subscribe((pages) => this.indexPages = pages);
  }

  pageClicked(index: number) {
    this.currentIndexPage = index;
    this.characters = this.loadingCharacters;
    this.getCharacters();
  }

}
