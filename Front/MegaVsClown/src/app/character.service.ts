import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Character from 'src/models/Character';
import { Observable } from 'rxjs';
import { API_PATH, playerName } from './global';




@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  


  constructor(private http: HttpClient) {
  }

  getCharacters(page: number): Observable<Character[]> {
    return this.http.get<Character[]>(`${API_PATH}/Character/GetCharacters?page=${page}`);
  }

  getPages(): Observable<number[]> {
    return this.http.get<number[]>(`${API_PATH}/Character/GetPages`);
}


}
