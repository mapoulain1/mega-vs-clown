port=7500
ss_line=$(ss -tulpn | grep LISTEN | grep $port)

if [[ $ss_line =~ pid=([0-9]*) ]]; then 
	old_server=${BASH_REMATCH[1]}    
	echo "old server still running"
	echo "killing server <$old_server>"
	kill -9 $old_server
fi

npm run build
serve -s dist/mega-vs-clown -l $port &
